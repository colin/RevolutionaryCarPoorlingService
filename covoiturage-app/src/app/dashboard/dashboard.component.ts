import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  private msg:string;

  constructor() { }

  ngOnInit() {
  		let user = localStorage.getItem('currentUser');
  		if(user){
  			let user_json = JSON.parse(user);
  			this.msg = "Welcome " +user_json.login;
  		}
  		else{
  			this.msg = "Welcome visitor";
  		}
  }

}
