import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {TrajectService} from './../services/traject.service';
import {Traject} from './../Model/Traject';
import {Injectable } from '@angular/core';

@Component({
  selector: 'app-traject',
  templateUrl: './traject.component.html',
  styleUrls: ['./traject.component.css']
})


export class TrajectComponent implements OnInit {

  id: string;
  private sub: any;
  private traject: Object;


  constructor(private route: ActivatedRoute, private trajectService:TrajectService) {}

  ngOnInit() {
  	this.sub = this.route.params.subscribe(params => {
       this.id = params['id']; // (+) converts string 'id' to a number
    });
    this.getTraject();
  }

  getTraject(){

  		this.trajectService.get(this.id).subscribe(
  			data => {
            if(data.success){
  				    this.traject = data.datas[0];
  				    console.log(this.traject);
            }
  			},
  			error => {
          		console.log("Oups Server error");
    	});
    	
  }

}
