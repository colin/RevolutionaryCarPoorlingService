import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrajectsearchComponent } from './trajectsearch.component';

describe('TrajectsearchComponent', () => {
  let component: TrajectsearchComponent;
  let fixture: ComponentFixture<TrajectsearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrajectsearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrajectsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
