import { Component, OnInit } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {RequestOptions} from '@angular/http';
import {HttpClient, HttpHeaders,HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

import {TrajectService} from './../services/traject.service';
import {Traject} from './../Model/Traject';

@Component({
  selector: 'app-trajectsearch',
  templateUrl: './trajectsearch.component.html',
  styleUrls: ['./trajectsearch.component.css']
})
export class TrajectsearchComponent implements OnInit {

  private trajects: Object[];

  constructor(private trajectService : TrajectService) { }

  ngOnInit() {
  	  this.buildTrajectList();
  }

  buildTrajectList(){
  		this.trajectService.getAll().subscribe(
  			data => {
            if(data.success){
  				     this.trajects = data.datas;
            }
  			},
  			error => {
          		console.log("Oups Server error");
    	});
    	
  }

}


