import { Component, OnInit } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {RequestOptions} from '@angular/http';
import {HttpClient, HttpHeaders,HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

import {UserService} from './../user.service';

import {User} from './../Model/User';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.css']
})
export class MyaccountComponent implements OnInit {

  private user:Object;

  constructor(private userService:UserService) { }

  ngOnInit() {
  	let user = localStorage.getItem('currentUser');
  	if(user){
  			let user_json = JSON.parse(user);
  			this.userService.get(user_json.login).subscribe(
  				data => {
  					if(data.success){
  						this.user = data.datas[0];
  						console.log(this.user);
  					}
  				},
  				error => {
          		console.log("Oups Server error");
    		});
  	}
  }

}
