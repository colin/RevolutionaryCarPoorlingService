import { Injectable } from '@angular/core';
import {RequestOptions} from '@angular/http';
import {HttpClient, HttpHeaders,HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {Traject} from './../Model/Traject';

@Injectable()
export class TrajectService {

  constructor(private http : HttpClient) {
  		
  }

  add(traject:Traject ): Observable<any> {

    let url = "http://localhost:8889/traject/add";
    let data = { traject:traject};
    let options = { headers: new HttpHeaders({ 'Content-Type': 'application/json'  }) };
    return this.http.post(url,data,options);
  }

  get(id:string): Observable<any> {
    let url = "http://localhost:8889/traject/get";
    let data = {"_id":id};
    let options = { headers: new HttpHeaders({ 'Content-Type': 'application/json'  }) };
    return this.http.post(url,data,options);
  }

  getAll(): Observable<any> {

    let url = "http://localhost:8889/traject/get";
    let data = { };
    let options = { headers: new HttpHeaders({ 'Content-Type': 'application/json'  }) };
    return this.http.post(url,data,options);
  }


}
