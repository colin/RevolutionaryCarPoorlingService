import { TestBed, inject } from '@angular/core/testing';

import { TrajectService } from './traject.service';

describe('TrajectService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrajectService]
    });
  });

  it('should be created', inject([TrajectService], (service: TrajectService) => {
    expect(service).toBeTruthy();
  }));
});
