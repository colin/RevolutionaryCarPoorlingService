import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // Forms 
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';

import {TrajectListComponent } from './traject-list/traject-list.component';
import {TrajectListService} from './traject-list/traject-list.service';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { AuthentificationComponent } from './authentification/authentification.component';
import { AuthentificationService } from './authentification/authentification.service';
import { RegisterformComponent } from './registerform/registerform.component';
import { RegisterformService } from './registerform/registerform.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import {UserService} from './user.service';
import { MenuComponent } from './menu/menu.component';
import { TrajectsearchComponent } from './trajectsearch/trajectsearch.component';
import { CreatetrajectComponent } from './createtraject/createtraject.component';
import { MyaccountComponent } from './myaccount/myaccount.component'

import {TrajectService} from './services/traject.service';
import { TrajectComponent } from './traject/traject.component';

@NgModule({
  declarations: [
    AppComponent,
    TrajectListComponent,
    AuthentificationComponent,
    RegisterformComponent,
    DashboardComponent,
    MenuComponent,
    TrajectsearchComponent,
    CreatetrajectComponent,
    MyaccountComponent,
    TrajectComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    TrajectListService,
    AuthentificationService,
    UserService,
    TrajectService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
