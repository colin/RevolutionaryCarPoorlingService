import {Address} from './Address';

export class Traject{

	constructor(
		private author_id:string,
		private start_city:string,
		private dest_city:string,

		private meet_address:Address,
		private meet_date:Date,
		private meet_time:string,
		private deposit_address:Address,
		private deposit_date:Date,
		private deposit_time:string,

		private place_nb:number,
		private price:number,
		private passengers:string[],
		private description:string
	 ) {}
}