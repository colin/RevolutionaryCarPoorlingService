
export class Address{

	constructor(
		private street_number:number,
		private street:string,
	 	private city: string,
	  	private state: string, 
	 	private place_name:string
	 ) {}
}