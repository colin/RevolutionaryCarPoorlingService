import {Address} from './Address';

export class User{
	
	constructor(
		private mail:string,
		private firstname:string,
		private lastname:string,
		private birth_date:Date,
		private address:Address,
		private phone_nb:string,
		private car_type:string
	) {}
}