    import { NgModule } from '@angular/core';
    import { RouterModule, Routes } from '@angular/router';

    import { DashboardComponent } from '../dashboard/dashboard.component';
    import { RegisterformComponent } from '../registerform/registerform.component';
    import { AuthentificationComponent } from '../authentification/authentification.component';
    import { CreatetrajectComponent } from '../createtraject/createtraject.component';
    import { TrajectsearchComponent } from '../trajectsearch/trajectsearch.component';
    import { MyaccountComponent } from '../myaccount/myaccount.component';
    import {TrajectComponent} from '../traject/traject.component';

    const routes: Routes = [
        {
            path: '',
            component: DashboardComponent,
        },
    	{
    		path: 'register',
    		component: RegisterformComponent,
    	},
        {   
            path: 'login',
            component: AuthentificationComponent,
        },
        {   
            path: 'createtraject',
            component: CreatetrajectComponent,
        },
        {   
            path: 'trajectsearch',
            component: TrajectsearchComponent,
        },
        {
            path: 'myaccount',
            component: MyaccountComponent,
        },
        {   path: 'traject/:id', 
            component: TrajectComponent,
        }

    ];

    @NgModule({

        imports: [RouterModule.forRoot(routes, {useHash: false})],

        exports: [
            RouterModule
        ],
        declarations: []
    })
    export class AppRoutingModule { }
