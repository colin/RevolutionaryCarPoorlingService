import { Injectable } from '@angular/core';
import {RequestOptions} from '@angular/http';
import {HttpClient, HttpHeaders,HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {User} from './Model/User';

@Injectable()
export class UserService {


  constructor(private http : HttpClient) {
  		
  }

  login(_login:string, _pwd:string) : Observable<any>{

    let url = "http://localhost:8888/user/login";
    let data = { login:_login, pwd : _pwd};
    let options = { headers: new HttpHeaders({ 'Content-Type': 'application/json'  }) };

    return this.http.post(url,data,options);
  }

  get(_login: string): Observable<any>{

  	let url = "http://localhost:8888/user/get";
  	let data = { login:_login};
  	let options = { headers: new HttpHeaders({ 'Content-Type': 'application/json'  }) };

   return this.http.post(url,data,options);
  }

  add(user : User, user_pwd:string): Observable<any> {

    let url = "http://localhost:8888/user/add";
    let data = {  user : user,  pwd : user_pwd };
    let options = { headers: new HttpHeaders({ 'Content-Type': 'application/json'  }) };

    return this.http.post(url,data,options);
  }

}
