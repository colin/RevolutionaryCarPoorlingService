import { TestBed, inject } from '@angular/core/testing';

import { TrajectListService } from './traject-list.service';

describe('TrajectListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrajectListService]
    });
  });

  it('should be created', inject([TrajectListService], (service: TrajectListService) => {
    expect(service).toBeTruthy();
  }));
});
