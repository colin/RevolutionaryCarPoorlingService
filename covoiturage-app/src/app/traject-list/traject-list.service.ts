import { Injectable } from '@angular/core';
import {RequestOptions} from '@angular/http';
import {HttpClient, HttpHeaders,HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {Traject} from '../Model/Traject';

@Injectable()
export class TrajectListService{

	constructor(private http : HttpClient) {
  		
  	}

  	add(traject : Traject): Observable<any> {

	    let url = "http://localhost:8889/Traject/add";
	    let data = {traject : traject};
	    let options = { headers: new HttpHeaders({ 'Content-Type': 'application/json'  }) };
	    return this.http.post(url,data,options);
  	}

  	register(traject_id:string, passenger_id:string) : Observable<any>  {
  		let url = "http://localhost:8889/Traject/add/passenger";
	    let data = {traject_id:traject_id, passenger_id:passenger_id};
	    let options = { headers: new HttpHeaders({ 'Content-Type': 'application/json'  }) };
	    return this.http.post(url,data,options);
  	}

  	/*get(filters_property:string[],filters_values:string[]) : Observable<any> {
  		if(! filters_property.length || (filters_property.length != filters_values.length)){
  			return;
  		}
  		var filter_json;
  		for(var i=0;i<filters_property.length;i++){
  			var property = filters_property[i];
  			var value = filters_values[i];
  			filter_json.property = value;
  		}
  		let url = "http://localhost:8889/Traject/get";
	    let data = {filters:filter_json};
	    let options = { headers: new HttpHeaders({ 'Content-Type': 'application/json'  }) };
	    return this.http.post(url,data,options);
  	}*/

}
