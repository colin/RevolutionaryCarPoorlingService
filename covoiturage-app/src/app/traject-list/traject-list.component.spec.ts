import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrajectListComponent } from './traject-list.component';

describe('TrajectListComponent', () => {
  let component: TrajectListComponent;
  let fixture: ComponentFixture<TrajectListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrajectListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrajectListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
