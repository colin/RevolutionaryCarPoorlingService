import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatetrajectComponent } from './createtraject.component';

describe('CreatetrajectComponent', () => {
  let component: CreatetrajectComponent;
  let fixture: ComponentFixture<CreatetrajectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatetrajectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatetrajectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
