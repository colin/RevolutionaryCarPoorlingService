import { Component, OnInit } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {RequestOptions} from '@angular/http';
import {HttpClient, HttpHeaders,HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

import {TrajectService} from './../services/traject.service'

import {Traject} from './../Model/Traject';
import {Address} from './../Model/Address';

@Component({
  selector: 'app-createtraject',
  templateUrl: './createtraject.component.html',
  styleUrls: ['./createtraject.component.css']
})


export class CreatetrajectComponent implements OnInit {

private is_logged:boolean;

private start_city:string;
private start_street_number: number;
private start_street: string;
private start_state:string;
private start_place_name:string;
private start_date:Date;
private start_time:string;

private dest_city:string;
private dest_street_number: number;
private dest_street: string;
private dest_state:string;
private dest_place_name:string;
private dest_date:Date;
private dest_time:string;

private place_nb:number;
private price:number;

private description:string;

private msg:string;


  constructor(private trajectService : TrajectService) { }

  ngOnInit() {
  	let user = localStorage.getItem('currentUser');
   
  	console.log(user);
  	this.is_logged = (user != null);
  }

  onSubmit(){
  	this._createatraject();
  }

  _createatraject(){

    let dest_hour = +(this.dest_time.split(':')[0]);
    let dest_minutes = +(this.dest_time.split(':')[1]);
    let start_hour = +(this.start_time.split(':')[0]);
    let start_minutes = +(this.start_time.split(':')[0]);

    let new_dest_date = new Date(this.dest_date);
    new_dest_date.setHours(dest_hour,dest_minutes,0);
    let new_start_date = new Date(this.dest_date);
    new_start_date.setHours(start_hour,start_minutes,0);

   
    //this.dest_date.setMinutes(minutes)
    let meet_address = new Address(this.start_street_number,this.start_street,this.start_city,this.start_state,this.start_place_name);
    let dest_address = new Address(this.dest_street_number,this.dest_street,this.dest_city,this.dest_state,this.dest_place_name);
    let user_id = JSON.parse(localStorage.getItem('currentUser')).login;

    let traject = new Traject(
    	user_id,
    	this.start_city,
    	this.dest_city,

    	meet_address,
    	new_start_date,
    	this.start_time,
    	dest_address,
    	new_dest_date,
    	this.dest_time,
    	this.place_nb,
    	this.price,
    	[],
    	this.description
    );
    

    this.trajectService.add(traject).subscribe(
        data => {
            if(!data.success){
                this.msg = data.datas;
                console.log(this.msg);
            }
            else{
                this.msg="Traject created with succes";
                alert("Trajet Validé !");
            }
        },
        error => {
          console.log("Oups Server error");
    });
    
  }
}
