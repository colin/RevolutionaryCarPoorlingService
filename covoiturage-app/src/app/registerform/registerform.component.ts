import { Component } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {RequestOptions} from '@angular/http';
import {HttpClient, HttpHeaders,HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

import {RegisterformService} from './registerform.service';
import {UserService} from './../user.service';

import {User} from './../Model/User';
import {Address} from './../Model/Address';

@Component({
  selector: 'app-registerform',
  templateUrl: './registerform.component.html',
//  window.location.href = 'home.html';
  styleUrls: ['./registerform.component.css']
})

export class RegisterformComponent {

  private firstname: string;
  private lastname: string;
  private mail: string;
  private phone_nb:string;
  private birth_date:string;
  private password: string;
  private password_confirmation: string;

  private street_number: number;
  private street: string;
  private city:string;
  private state:string;
  private place_name:string;

  private accountCreated: boolean = false;
  private selectors: Object; 

  private msg:string;


  constructor(private userService : UserService) { }


  onSubmit(){
  	this._createaccount();
  }

  _createaccount(){

    let address = new Address(this.street_number,this.street,this.city,this.state,this.place_name);

    let user = new User(this.mail,this.firstname,this.lastname,new Date(this.birth_date),address,this.phone_nb,"");
    this.userService.add(user,this.password).subscribe(
        data => {
            if(!data.success){
                this.msg = data.datas;
                console.log(this.msg);
            }
            else{
                this.msg="User created with succes";
            }
        },
        error => {
          console.log("Oups Server error");
    });
    
  }

}
