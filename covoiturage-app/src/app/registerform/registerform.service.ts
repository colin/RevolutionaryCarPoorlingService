import {RequestOptions} from '@angular/http';
import {HttpClient, HttpHeaders,HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

@Injectable()
export class RegisterformService {

private datas: Object[];

 constructor(private http : HttpClient) {}

    check(firstname:string, lastname:string, login:string, password:string, passwordconfirm:string): Observable<any> {

    let url = "http://localhost:8888/register";
  	let data = {"firstname": firstname, "lastname": lastname, "login": login, "password": password, "passwordconfirm":passwordconfirm};
  	//let headers = new Headers({ 'Content-Type': 'application/json' });
  	//let options = new RequestOptions({headers: headers, url: url});
  	let options= {
  		headers: new HttpHeaders({ 'Content-Type': 'application/json' , 'Access-Control-Allow-Origin' :'*'})
	};
    let observable: Observable<any> = this.http.post(url,data).map((res:Response) => res.json());
	return observable;
  }

}
