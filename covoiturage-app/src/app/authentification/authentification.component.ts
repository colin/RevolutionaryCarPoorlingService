import { Component } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser'

import {RequestOptions} from '@angular/http';
import {HttpClient, HttpHeaders,HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';


import {AuthentificationService} from './authentification.service';
import {UserService} from './../user.service';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.css']
})

export class AuthentificationComponent {

  private login: string;
  private password: string;
  private isLoggedIn: boolean = false;
  private selectors: Object;
  private msg:string;

  constructor(private userService : UserService) { }


  onSubmit(){
  	this._login();
  }

  _login(){
    
    this.userService.login(this.login,this.password).subscribe(
        data => {
            if(!data.success){
                this.msg = data.datas;
            }
            else{
                this.isLoggedIn = true;
                localStorage.setItem('currentUser',data.datas);
                
            }
        },
        error => {
          console.log(error);
          console.log("Oups Server error");
        });

  }

  _logout(){
  	this.isLoggedIn = false;
  }

}
