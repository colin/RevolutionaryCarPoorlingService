var express = require('express');
var fs      = require('fs');
var app = express();
var bodyParser = require('body-parser');
cors = require('cors')
app.use(cors()); // handle CORS request 
app.use(bodyParser.json()); // support json encoded bodies

var async = require('async');

var jwt        = require("jsonwebtoken");
const jwt_expiration_time = 1800; // 1800s -> 30min
//var mongoose   = require("mongoose");

var crypto = require('crypto');
var bcrypt = require('bcrypt'); // bcrypt for hashing
const bcrypt_salt_round = 14;

var MongoClient = require("mongodb").MongoClient; // mongoDB noSQL
var assert = require("assert");

var dbName = 'Carpooling';
const db_url = "mongodb://127.0.0.1:27017";

var user_collection = "User";
var address_collection = "Address";
var pwd_collection = "Pwd";
var user_token_collection ="UserToken";


function send_datas(res,is_success,_datas){
	res.setHeader('Content-Type', 'application/json');
	res.setHeader('Access-Control-Allow-Origin','*');
	res.status(200).json({
		success:is_success,
		datas:_datas
	});
}



MongoClient.connect(db_url,function(err,client) {
	assert.equal(null,err);
	console.log("Connection to user db");
	const db = client.db(dbName);

	/**
	*
	*
	*
	*/

	app.post('/user/add',function(req,res) {
		console.log('/user/add');
		if(req.body.user == undefined || req.body.pwd == undefined) // check input
			return res.send(400,'Bad Input');
		var user = req.body.user; //check user validating scheme
		var plain_pwd = req.body.pwd;

		async.waterfall([
			function(callback){ // check if user already exist 
				db.collection(user_collection).findOne({mail:user.mail}, function(user_db_err, existing_user) {
					if(user_db_err)
						return res.send(500,res,user_db_err);
					if(existing_user)
						return send_datas(res,false,'User already exist');
					else
						callback(null);
					
				});
			},
			function(callback){ // insert new user 
				db.collection(user_collection).insertOne(user,function(user_db_err,inserted_user){
					if(user_db_err ) 
						return res.send(500,user_db_err);
					callback(null)
						 
				});
			},
			function(callback){ // generate hash
				bcrypt.hash(plain_pwd,bcrypt_salt_round, function(bcrypt_err, hash) {
					if(bcrypt_err)
						return res.send(500,bcrypt_err);
					callback(null,hash);
				});
			},
			function(hash,callback){ // insert hash into pwd db
				db.collection(pwd_collection).insertOne({login:user.mail,pwd:hash},function(pwd_db_err,inserted_pwd){ // insert into pwd_collectiob
					if( pwd_db_err)
						return res.send(500,pwd_db_err); 
					console.log("User added");
					send_datas(res,true,'');
					callback(true);
				});
			}],
			function(){
				return;
			}
		);
	});

	/**
	*
	*
	*
	*/
	app.post('/user/get',function(req,res) {
		console.log('/user/get');
		console.log(req.body.login);

		if(!  req.body.login)
			return res.status(400).send("Bad input");

		var login = req.body.login;
		db.collection(user_collection).find({"login":login}).toArray(function(user_db_err,datas){ // find user into db
			if(user_db_err)
				return res.status(500).send(user_db_err);
			if(! datas) 
				return send_datas(res,true,JSON.stringify({}));
			send_datas(res,true,JSON.stringify(datas));
		});

	});


	/**
	*
	*
	*
	*/
	app.post('/user/login',function(req,res) {
		console.log('/user/login');
		if( ! req.body.login || ! req.body.pwd )
			return res.status(400).send("Bad input");
		var login = req.body.login;
		var pwd = req.body.pwd;
		console.log(req.body);
		async.waterfall([

			function(callback){ // check user existence 

				db.collection(pwd_collection).findOne({"login":login},function(pwd_db_err,existing_user){
					if(pwd_db_err)
						return res.send(500,pwd_db_err);
					if(! existing_user) { // user not found into db
						return send_datas(res,false,'Invalid user/password');
					}
					else
						callback(null,existing_user);
				});
			},

			function(existing_user,callback){   // compare hash
				
				bcrypt.compare(pwd,existing_user.pwd,function(bcrypt_err,hash_res){
					if(bcrypt_err) 
						return res.send(500,bcrypt_err);
					if(! hash_res){
						return send_datas(res,false,'Invalid user/password'); 
					}
					else
						callback(null,existing_user);
				});
			},

			function(existing_user,callback){ // generate token with random bytes 
				crypto.randomBytes(48,function(crypto_err,random_secret){ // generate random byte

					if(crypto_err)
						return res.send(500,crypto_err);
					// // generate token with expiration date
					jwt.sign({token:existing_user._id},random_secret.toString('hex'),{expiresIn:jwt_expiration_time},function(jwt_err,token){
						if(jwt_err)
							return res.send(500,res,jwt_err);
						callback(null,login,token);
					});
				});
			
			},

			function(login,token,callback){ // store token into db
				db.collection(user_token_collection).replaceOne({login:login},{login:login,token:token},function(token_db_err,inserted_token){
					if(token_db_err)
						return res.send(500,res,token_db_err);
					send_datas(res,true,JSON.stringify({login:login,token:token}));
					console.log("User logged");
					callback(true);
				});
			}],

			function(){
				return;
			}
		);

					
	});

});
	

app.listen(8888);