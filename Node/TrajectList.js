var express = require('express');
var fs      = require('fs');
var app = express();
var bodyParser = require('body-parser');
cors = require('cors')
app.use(cors()); // handle CORS request 
app.use(bodyParser.json()); // support json encoded bodies

var async = require('async');


var MongoClient = require("mongodb").MongoClient; // mongoDB noSQL
var assert = require("assert");

var dbName = 'Carpooling';
const db_url = "mongodb://127.0.0.1:27017";

var user_collection = "User";
var address_collection = "Address";
var pwd_collection = "Pwd";
var user_token_collection ="UserToken";
var traject_collection = "Traject";


function send_datas(res,is_success,_datas){
	res.setHeader('Content-Type', 'application/json');
	res.setHeader('Access-Control-Allow-Origin','*');
	res.status(200).json({
		success:is_success,
		datas:_datas
	});
}

MongoClient.connect(db_url,function(err,client) {
	assert.equal(null,err);
	console.log("Connection to TrajectList db");
	const db = client.db(dbName);


	app.post('/traject/get',function(req,res){
		console.log('/traject/get');
		var filters;
		if(req.body.filters){
			filters = req.body.filters;
		}
		//filters = { price:{$lte:20},dest_city:'Montpellier' };
		db.collection(traject_collection).find(filters).toArray(function(traject_db_err,trajects){
			if(traject_db_err){
				return res.status(500).send(traject_db_err);
			}
			if(! trajects){
				return send_datas(res,false,JSON.stringify({})); // no traject found
			}
			return send_datas(res,true,trajects);

		});
	});

	app.post('/traject/add',function(req,res){
		console.log('/traject/add');
		if(! req.body.traject){
			return res.status(400).send('Bad input'); // check if traject is a traject
		}
		var traject = req.body.traject;
		db.collection(traject_collection).insertOne(traject,function(traject_db_err,inserted_traject){
			if(traject_db_err){
				return res.status(500).send(traject_db_err);
			}
			console.log('Traject added');
			return send_datas(res,true,'');
		});
	});

	app.post('traject/add/passenger',function(req,res){
		if(! req.body.traject_id || ! req.body.passenger_id)
			return res.status(400).send('Bad input: no traject or passanger');
		var traject_id = req.body.traject_id;
		var passenger_id = req.body.passenger_id;
		db.collection(traject_collection).findOne({_id:ObjectId(traject_id)}).toArray(function(traject_db_err,traject){
			if(traject_db_err ){
				return res.status(500).send(traject_db_err);
			}
			if(! passengers){
				return res.status(400).send('Bad input:traject not found');
			}
			var passengers = traject.passengers;
			if( (traject.place_nb - passengers.length) == 0){ // no place available
				return res.status(400).send('Bad input:No place available');
			}
			passengers.push(passenger_id);
			db.collection(traject_collection).updateOne({_id:traject_id},{passengers:passengers},function(traject_db_err,new_traject){
				if(traject_db_err){
					return res.status(500).send(traject_db_err);
				}
				return send_datas(res,true,'');
			});
		});
	});
});

app.listen(8889);